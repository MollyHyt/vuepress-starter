# 開始一個 VuePress 專案吧！

凡事都從最簡單基礎的架構開始體驗吧，第一步就是先確認電腦上有沒有相應的開發環境，因為 VuePress 是使用 Webpack 來進行模組處理的，而且目前前端各項工具基本上都離不開 Node.js 的執行環境，所以要先進行安裝，如果已經安裝的也要確認版本喔。

## 1. 安裝 [Node.js](https://nodejs.org/en/)

VuePress 需要 Node.js >= 8.6

如果沒有安裝過 Node.js 的話，可能你目前不是專精於前端領域的開發者，先到 [Node.js](https://nodejs.org/en/) 官方網站下載 LTS 版本安裝即可，建議安裝 LTS 版本為 12.15.3

推薦使用 v12.x，因為
– VuePress 是在 v12.x 下開發的
– 最新版的 v14.x 經測試，雖然可以執行，但 Yarn 有幾個相依套件宣稱與 v14.x 不相容

Node.js 中有內建一個叫做 `npm` 的套件管理工具，但是因為 VuePress 官方建議使用 `Yarn` 進行套件管理，所以還需要打開終端機視窗輸入下面的指令安裝 Yarn：

```bash
npm install -g yarn
yarn -v
1.22.19
```

該裝的環境安裝完，就可以進行下一步囉！

## 建立專案資料夾

不管你是喜歡下指令，還是喜歡用滑鼠點，都沒有關係，反正有辦法建立出一個專案資料夾就可以了，但是資料夾建立好之後，還是得靠指令了喔！

一樣打開終端機，先進入剛剛建立好的資料夾輸入指令 `yarn init`

專案初始化的設定可以不用管，先都用預設值一路 `Enter` 就好了

完成後資料夾內會多一個 `package.json` 的檔案，這個檔案會記錄專案內用到的套件與指令，接著我們就來安裝第一個套件 `VuePress` 吧！

```bash
yarn add -D vuepress
```

接著要在剛剛提過的 `package.json` 中新增一段腳本，透過腳本的運行環境才能順利執行 VuePress 的各項指令喔：

```json
"scripts": {
    "dev": "vuepress dev ./",
    "build": "vuepress build ./"
},
```

貼上之後的 `package.json` 大致上會長這樣：

```json
{
  "name": "vuepress-sample",
  "version": "1.0.0",
  "main": "index.js",
  "license": "MIT",
  "scripts": {
    "dev": "vuepress dev ./",
    "build": "vuepress build ./"
  },
  "devDependencies": {
    "vuepress": "^1.9.7"
  }
}
```

## 新增第一份文件啦

直接在專案根目錄中新增一個 `README.md` 檔案，檔案裡面可以使用 Markdown 的語法先隨便輸入一些內容
![https://i.imgur.com/FEL64kC.png](https://i.imgur.com/FEL64kC.png)

接著就可以到終端機視窗，輸入指令 `yarn dev` 來運行開發模式囉！

輸入完等子彈飛一會，沒有意外的話就會看到像這樣的畫面：

![https://i.imgur.com/VQM93u8.png](https://i.imgur.com/VQM93u8.png)

這時候我們就可以打開瀏覽器連線到 [`http://localhost:8080/`](http://localhost:8080/) 來確認一下運行結果啦！
![img](https://i.imgur.com/BYjc53D.png)

可以看到剛剛我們在 `README.md` 中輸入的內容已經呈現在網頁上了，而且如果有安裝 `Vue.js devtools` 的話，還可以看到組件與狀態喔。

## 新的頁面分類

當然網站也不可能只有一個頁面對吧？

我們就來試試看新增一個資料架 `news`，裡面新增兩個檔案：

- `README.md`
- `test.md`
  ![img](https://i.imgur.com/Wgz2PoP.png)

儲存完之後我們再到網頁上面看看，網址列直接輸入 [`http://localhost:8080/news/`](http://localhost:8080/news/) 就會顯示我們剛剛在 `news` 資料夾中 `README.md` 的內容

這個是因為 VuePress 預設將資料夾內的 `README.md` 視為根目錄文件喔。

所以如果想到看到 `test.md` 的內容，就只能輸入完整的路徑 http://localhost:8080/news/test.html 囉。

而這個 `.html` 則是因為 VuePress 將 `.md` 編譯為 HTML 文件的關係。

> 其實 VuePress 預設的主題內就有搜尋功能，也是可以趁機玩玩看啦！
> ![img](https://i.imgur.com/3deYio2.png)

如果想要調整頁面的樣式或是加入客製化的功能，該怎麼做呢？

------

## 在專案中建立 `.vuepress` 進階設定資料夾

只要在專案中建立如下的資料夾結構，就可以透過各項設定讓 VuePress 網站符合你的需求囉！

```jsx
.
├── .vuepress (可選的)
│   ├── components (可選的)
│   ├── theme (可選的)
│   │   └── Layout.vue
│   ├── public (可選的)
│   ├── styles (可選的)
│   │   ├── index.styl
│   │   └── palette.styl
│   ├── templates (可選的, 謹慎配置)
│   │   ├── dev.html
│   │   └── ssr.html
│   ├── config.js (可選的)
│   └── enhanceApp.js (可選的)
│ 
├── README.md
│ 
├── news
│   ├── README.md
│   └── test.md
│ 
├── config.js
│ 
├── yarn.lock
│ 
└── package.json
```

> 要注意一下命名喔，名稱要完全正確都是小寫才能「符合約定」

你有注意到嗎？其實包含 `.vuepress` 本身在內，內層的所有資料夾也都是「選用」的喔，沒有哪個檔案或目錄是必要的。

基本上就是一個「複寫」的概念，如果有需要特別設定的部分，再新增檔案來複寫就可以了，沒有的話就會按照預設值來跑，所以也不會造成錯誤。





##### Push an existing folder

```
cd existing_folder
git init
git remote add origin git@twtpeamssw07.deltaos.corp:MOLLY.H/vuepress-starter.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

