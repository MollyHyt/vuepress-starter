module.exports = {
  title: 'Hello VuePress',
  description: 'Just playing around',
  // 注意前後都要有 / ，假如你在 GitLab 上用的專案名稱不是 vuepress-starter，
  // 要置換成你自己的專案名稱
  base: '/vuepress-starter/',
  // public 這個名稱是固定的
  dest: 'public',
  locales: {
    '/': {
      lang: 'zh-TW'
    }
  },
  theme: 'reco',
  themeConfig: {
    nav: [
      { text: '首頁', link: '/' },
      { 
        text: 'NAV',
        items: [
          { text: 'Github', link: 'https://github.com/mqyqingfeng' },
          { text: 'VuePress搭建部落格', link: 'https://tw511.com/a/01/41131.html' }
        ]
      }
    ],
    sidebar: [
      {
        title: '歡迎學習',
        path: '/',
        collapsable: false, // 不折疊
        children: [
            { title: "學前必讀", path: "/" }
        ]
      },
      {
        title: "基礎學習",
        path: '/handbook/ConditionalTypes',
        collapsable: false, // 不折疊
        children: [
          { title: "條件型別", path: "/handbook/ConditionalTypes" },
          { title: "泛型", path: "/handbook/Generics" }
        ],
      }
    ]
  }
}
