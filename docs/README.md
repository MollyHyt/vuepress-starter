# VuePress Doc

Example VuePress website using GitLab Pages. https://mollyhyt.gitlab.io/vuepress-starter/

Learn more about GitLab Pages at https://pages.gitlab.io and the official documentation https://docs.gitlab.com/ce/user/project/pages/.



## Requirement

- Node 12, Node 14 will get some function that doesn't work

```
$ node -v
v12.22.12
```

## Building Locally

1. 將 VuePress 安裝為本地依賴
   ```
   $ yarn add -D vuepress    # npm install -D vuepress

   ```

2. 本地啟動伺服器
   ```
   yarn docs:dev    # npm run docs:dev
   ```
   VuePress 會在 http://localhost:8080 (opens new window) 啟動一個熱過載的開發伺服器。


### Use mermaid.js Diagrams in VuePress

[mermaid 官網](https://mermaid-js.github.io/mermaid/#/)

https://vuepress-theme-hope.github.io/v1/zh/guide/get-started/markdown.html#tex-%E8%AF%AD%E6%B3%95

[VuePress Plugin for mermaid.js](https://github.com/eFrane/vuepress-plugin-mermaidjs/tree/1.x)

This VuePress plugin provides a global component wrapping mermaid.js. The plugin is designed to work with VuePress v1.x.

Main documentation site is at vuepress-plugin-mermaidjs.efrane.com.


```mermaid
sequenceDiagram

Client -> NBI: 1.access token with username and password
note over NBI: add authorization code
NBI -> Auth: 1.1 forwarding request
note over Auth: authenticate user and generate token
Auth -> NBI: 1.2 response access & refresh token
NBI -> Client: 1.3 forwarding response
note over Client: get token
Client -> NBI:2.call api with access token
NBI -> Auth: 2.1 check token and expiration
Auth -> NBI: 2.2 authorization is correct
NBI --> Micro service: 2.3 forwarding request
Micro service --> NBI:2.4 response with data
NBI -> Client: 2.5 forwarding response
note over Client: get data
note left of Client: access token expires
Client -> NBI: 3.refresh token
NBI -> Auth: 3.1 forwarding request
note over Auth: authenticate refresh token and update access token
Auth -> NBI: 3.2 response access & refresh token
NBI -> Client: 3.3 forwarding response
note over Client: get new token
```
