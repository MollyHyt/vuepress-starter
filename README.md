# Hello VuePress

這不是真首頁, docs/README.md 才是

生成後的頁面在 https://mollyhyt.gitlab.io/vuepress-starter/

# 安裝

## 安裝 Node.js

推薦使用 v12.x，因為

- VuePress 是在 v12.x 下開發的
- 最新版的 v14.x 經測試，雖然可以執行，但 Yarn 有幾個相依套件宣稱與 v14.x 不相容

```bash
$ nvm use 12
Now using node v12.22.12 (npm v6.14.16)
$ nvm install 12
$ node -v
v12.22.12
```

## 安裝 Yarn

使用 npm，Node.js 內附的套件管理器，來安裝另外一個套件管理器

```bash
npm install -g yarn
```

## 專案安裝 VuePress

這個過程中會產生 yarn.lock 檔案，包含了所有相依套件的版本，推薦加入到 git 管理

```bash
$ yarn add -D vuepress
$ yarn docs:dev
success [16:20:45] Build ef7bef finished in 2008 ms! ( http://localhost:8080/vuepress-starter/ )
```

VuePress 會啟動可以”即時更新”的伺服器 (真的是即時更新喔！可以修改首頁試試看)

移至 <http://localhost:8080> 可以看到剛剛做的首頁

按 Ctrl+C 來停止伺服器

### 參考

- <https://deltatimer.com/812/vuepress-gitlab-pages>
